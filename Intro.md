# Introduction

### Teachers

- Richard Calvo
- David L. Willson

---

### Software Freedom School

Welcome to the Software Freedom School.

Our purpose here is to teach one another how to use and why to choose Free Software.

---

#### Free Software Definition

Free Software is software that you are free to:

- **Use** for whatever purpose you see fit.
- **Study** how it works, for which access to the source code is a pre-requisite
- **Share** with your friends.
- **Modify** to better suit your needs.

In sum, Free Software authors respect your freedom to do art and science with the software.

That seems like a good reason to prefer Free Software over non-free, proprietary software.

---

#### Libre Not Gratis - Request For Payment / Pay What You Choose

SFS class materials are free-libre *not* free-gratis.

The ask for today's class is: 16 USD

You may pay the ask or Pay What You Choose: more, less, or differently.

Common ways to Pay What You Choose:

- Pay with **crypto-currency**
- Send a **postcard** with a picture of you saving a whale or a kitten.
- **Teach** a class. This is our favorite, and you can make money at it!

---
