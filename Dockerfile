# https://hub.docker.com/_/microsoft-dotnet
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
ENV ASPNETCORE_ENVIRONMENT=Development
WORKDIR /source

# copy csproj and restore as distinct layers
COPY *.sln .
COPY myfirstapi/*.csproj ./myfirstapi/
WORKDIR /source/myfirstapi
RUN dotnet restore -r linux-x64
WORKDIR /source

# copy everything else and build app
COPY myfirstapi/. ./myfirstapi/
WORKDIR /source/myfirstapi
RUN dotnet publish -c debug -o /app -r linux-x64 --self-contained false --no-restore

# final stage/image
FROM mcr.microsoft.com/dotnet/aspnet:6.0
ENV ASPNETCORE_ENVIRONMENT=Development
WORKDIR /app
COPY --from=build /app ./

ENTRYPOINT ["dotnet", "myfirstapi.dll"]