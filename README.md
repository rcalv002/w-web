<!-- Change the title of the class -->

# W is for Web Services

---

### Class Goal

The web is everywhere these days. You want to find directions? Web. You want to order food? Web. You want to take a class? Web.
You want to find that obscure detail or fact-check your smack talking buddy? Web.

But how does my browser or phone end up displaying those silly memes for me? In reality, there are many complex pieces that are stitched together like threads in a web (I know right?) to give you access to the information and entertainment you want.

In this class we'll be covering *web services*: their construction and consumption

---

### Success Metric

The ability to understand what happens when you `request` information, and how those `responses` make their way back to you. We'll end up with a *simple* working application that allows us to make requests to a server.

---

### Prerequisites

- Familiarity with a terminal
- Patience
  - Something with a Bash-like shell is preferred (Mac or Linux)
  - Install these tools before you arrive:
    - [Docker](https://docs.docker.com/get-docker/)
    - Some kind of Remote Desktop Client
- [Create a GitLab account](https://gitlab.com/users/sign_in)
  - If you already have one, you can disregard this
  - If you create a new one, you might consider making it something that you could take from company to company

---

### Following Instructions

Please follow the documented and stated instructions as closely as possible.  This will help mitigate issues that arise due to funky configurations.  The labs for this class are very inter-dependent, and an innocent deviation in an early lab could complicate a later lab.

I encourage all students to experiment and explore the material.  Making it your own and having fun with it increases the functional utility of this class immensely.  I'm happy to help with any extracurricular questions and/or interests related to the material, but please try the extra stuff *after* completing the suggested stuff.  And maybe in a different subdirectory.  :)

---

### Environment Setup

See [this repo](https://gitlab.com/rcalv002/sfsdockers/-/tree/main/container-xrdp) with instructions on how to build the docker image we'll be using to launch our dev container

Once you've downloaded the docker, built the image and launched the container, connect to it via RDP

---

## Labs

<!-- OPTIONAL: Create a Table of Contents. -->
<!-- Make sure to keep this up to date as you make edits. -->
<!-- If you opt not to do this, a link to the first lab is helpful. -->

### 1. [Project setup](labs/01_project_setup/Readme.md)

### 2. [Controllers](labs/02_controllers/Readme.md)

### 3. [Persistence](labs/03_persistence/Readme.md)

### 4. [Restricting Data Transfers](labs/04_dto/Readme.md)

---

### Contributing

Feel free to submit an Issue or open a Merge Request against this repository if you notice any mis-spellings, glaring omissions, or opportunities for improvement.
