# Restricting Data Transfers

In the previous lab we set up our access to a database. Although our domain model in this class is simple, image a situation where you have very large tables, maybe even sensitive data. We should not transfer all of this data that exists in database tables over our api, instead we should use
[Data Transfer Objects](https://docs.microsoft.com/en-us/aspnet/web-api/overview/data/using-web-api-with-entity-framework/part-5), skimmed down versions of our models to transfer only what we need and to restrict inputs.

---

## Environment

We'll be starting this lab where we left off the previous one inside `/home/<youruser>/TodoApi`. You should already have VSCodium open but if you don't go ahead and open it in this path

```bash
codium .
```

---

### Track it

Lets create our Lab04_start branch

```bash
git checkout -b lab04_start
```

### Creating our Models

Let's go to our existing `TodoItem` model and add an additional property `Social` that will help us understand the concept.

Browse to the `Models` folder and open the `TodoItem.cs` file. Add the property Social at the end

```csharp
public bool IsComplete { get; set; }  
```

After:

```csharp
namespace TodoApi.Models
{
    public class TodoItem
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public bool IsComplete { get; set; }
        public string? Social { get; set; }
    }
}
```

Commit this as our starting point

```bash
git add .
git commit -m "Added Social property to TodoItem model"
```

In the same Models folder add three new files, each representing a specific input on our controller routes.

Click the `Models` folder and press the File+ icon to add the files below

- `TodoItemDTO.cs`
- `TodoItemCreateDTO.cs`
- `TodoItemUpdateDTO.cs`

Paste the code below as appropriate and save them.

- `TodoItemDTO.cs`

```csharp
namespace TodoApi.Models
{
    public class TodoItemDTO
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public bool IsComplete { get; set; }
    }
}
```

- `TodoItemCreateDTO.cs`

```csharp
namespace TodoApi.Models
{
    public class TodoItemCreateDTO
    {
        public string? Name { get; set; }
        public bool IsComplete { get; set; }
        public string? Social { get; set; }
    }
}
```

- `TodoItemUpdateDTO.cs`

```csharp
namespace TodoApi.Models
{
    public class TodoItemUpdateDTO
    {
        public string? Name { get; set; }
        public bool IsComplete { get; set; }
    }
}
```

Notice how we can `create` a new `TodoItem` by sending in the name, whether or not its complete, and a Social #. However, when we `get` the `TodoItem` we can no longer see the Social, even though its stored in the database. The only properties we can update on an existing TodoItem is the Name and the IsComplete properties.

#### Use our Models

Even though we've created these models, our application doesn't know that it should use them. We have to update our controller to expect data in the shape of our models at the appropriate route.

Open the `TodoController.cs` file in the `Controllers` folder and replace the content so it looks like below.

```csharp
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoApi.Models;

namespace TodoApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TodoController : ControllerBase
{
    private readonly ILogger<TodoController> _logger;
    private readonly TodoContext _dbcontext;

    public TodoController(ILogger<TodoController> logger, TodoContext dbcontext)
    {
        _logger = logger;
        _dbcontext = dbcontext;
    }

    [HttpGet()]
    public async Task<ActionResult<IEnumerable<TodoItemDTO>>> GetTodoItems()
    {
        return await _dbcontext.TodoItems
            .Select(x => ItemToDTO(x))
            .ToListAsync();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<TodoItemDTO>> GetTodoItem(long id)
    {
        var todoItem = await _dbcontext.TodoItems.FindAsync(id);
        
        if (todoItem == null)
        {
            return NotFound();
        }

        return ItemToDTO(todoItem);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateTodoItem(long id, TodoItemUpdateDTO todoItem)
    {

        var existingItem = await _dbcontext.TodoItems.FindAsync(id);
        if (existingItem == null)
        {
            return NotFound();
        }

        existingItem.IsComplete = todoItem.IsComplete;
        existingItem.Name = todoItem.Name;

        _dbcontext.Entry(existingItem).State = EntityState.Modified;

        try
        {
            await _dbcontext.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!TodoItemExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }

        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<TodoItem>> DeleteTodoItem(long id)
    {
        var todoItem = await _dbcontext.TodoItems.FindAsync(id);

        if (todoItem == null)
        {
            return NotFound();
        }

        _dbcontext.TodoItems.Remove(todoItem);
        await _dbcontext.SaveChangesAsync();

        return NoContent();
    }

    [HttpPost]
    public async Task<ActionResult<TodoItemDTO>> CreateTodoItem(TodoItemCreateDTO todoItem)
    {
        TodoItem todo = new TodoItem()
        {
            Name = todoItem.Name,
            IsComplete = false,
            Social = todoItem.Social
        };

        _dbcontext.TodoItems.Add(todo);
        await _dbcontext.SaveChangesAsync();

        return CreatedAtAction(nameof(GetTodoItem), new { id = todo.Id }, ItemToDTO(todo));
    }

    private bool TodoItemExists(long id){
        return _dbcontext.TodoItems.Any(x => x.Id == id);
    }

    private TodoItemDTO ItemToDTO(TodoItem todoItem)
    {
        return new TodoItemDTO
        {
            Id = todoItem.Id,
            Name = todoItem.Name,
            IsComplete = todoItem.IsComplete
        };
    }
}

```

We did several things here

- When retrieving a TodoItem, we no longer return the entire TodoItem as stored in the database, we return the dto, which can have a subset of properties to hide information or save bandwidth
- When updating a TodoItem we only allow updating specific properties

---

### Testing our changes

Start the project by going to the Debug tab and press play! If it was already running, stop it and start it again.
Open a browser to http://localhost:5000/swagger
In the swagger UI

- Expand the GET api/Todo
- Click Try it Out
- Click Execute

You should receive a success response (200) with an empty array []. That's because the database is currently empty. Press cancel and collapse the tab.

Let's add a Todo item:

- Expand the POST api/Todo
- Click Try it Out
- Change the name property and click Execute

You should receive a 201 Successfully Created response with the content you sent in the payload.

Let's update the Todo item:
- Expand the PUT api/Todo
- Click Try it Out
- Provide the ID of the previously created Todo item
- Change the IsComplete property and click Execute

You should receive a 204 NoContent response

---

### Commit our changes

If the application is running, go back to the Debug tab on VSCodium and press stop.

Since we want to have a starting and ending state for each lab for reference, let's go ahead and create a new branch and commit it with this final state of lab04

```bash
git checkout -b lab04_end
git add .
git commit -m "Lab04 finished"
```

<!-- OPTIONAL: References and Links -->
### References & Links

- [Data Transfer Objects](https://docs.microsoft.com/en-us/aspnet/web-api/overview/data/using-web-api-with-entity-framework/part-5)

---

<!-- OPTIONAL: Include a navigator at the bottom -->
<!-- This is very user-friendly, but a pain to maintain -->

|Previous: [Persistence](/labs/03_persistence/Readme.md)|Next: [Bonus](/labs/01_project_setup/Readme.md)|
|---:|:---|
