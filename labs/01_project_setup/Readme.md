# Project Setup

Now that we're connected to our development environment, we can generate our dotnet project. We'll use the dotnet CLI which generates a starter template for us based on the type we want. This is our starting point for the entire api!

---

<!-- OPTIONAL Section -->
## Environment

We'll be working out of our `$HOME` or `~` directory so go ahead and drop a `cd` in the prompt to move to your home directory. It should look something like `<youruser>@<containerid>`. Typing `pwd` should return something like `/home/<youruser>`

---

<!-- Update this section name -->
### Create solution folder

<!--
As you demo...
  explain what you're doing in each step
  explain what's happening in each step
If relevant...
  explain where each step fits into the overall workflow
  explain where the action is happening (e.g. local vs. remote web server)
-->

1. Set global git configuration for future git commands as below

```bash
git config --global user.email "rcalv002@sfsweb.class"
git config --global user.name "Richard Calvo"
```

2. Create a solution folder named `sfsweb` where we will generate our template and cd into it. We should then be in `/home/<youruser>/sfs`

```bash
mkdir sfsweb
cd sfsweb
```

3. Run `git init` here to initialize an empty git project

```bash
git init
# should show:
#   Initialized empty Git repository in /home/<youruser>/sfsweb/.git/

# Create .gitignore and commit it so the master branch is created
# We want to ignore some directories in our project that change often on build.
echo -e "TodoApi/obj/\nTodoApi/bin/" >> .gitignore

git add .gitignore
git commit -m "initial"
git status
# should show:
#   On branch master
#   nothing to commit, working tree clean
```

4. Create a new branch to store our lab01 starting state and switch to it

```bash
git checkout -b lab01_start
# should show:
#   Switched to branch 'lab01_start'
```

5. Generate a dotnet project called TodoApi using the `webapi` template. This uses the dotnet cli to create a folder for us based on a template. This folder then has all of the necessary startup files to run a basic web api .net application.

```bash
dotnet new webapi -o TodoApi
# should show:
#   The template "ASP.NET Core Web API" was created successfully.
#   Restore succeeded.
```

6. Commit the current state with message "Generated TodoApi Project" or something relevant

```bash
git add TodoApi/
git commit -m "Generated TodoApi project"
```

7. Switch into the TodoApi folder and open it with VSCodium, our visual editor; if you receive a warning, accept it by clicking `Yes I trust the authors`.

  ```bash
  cd TodoApi/
  codium .
  ```

![Initial VSCodium Window](images/CodiumInit.png)

8. Back on the terminal you can run `dotnet run` to run the application. **Take note of the http listening address as we'll use it in the future**

  ```bash
  dotnet run
  # should show:
  # Building... and then some debug logs that the API is listening
  ```

![Sample dotnet run output](images/dotnetrunsample.png)

---

<!-- OPTIONAL: Repeat as necessary -->
<!-- More than three logical chunks in a lab is a yellow flag -->
<!-- A lab with 4+ chunks could probably be split into multiple labs -->
<!-- Update this section name -->
### Install VSCodium Extensions

We want an easy way to test our api calls, so instead of using curl or wget we'll install the Thunder extension in our VSCodium, which allows us to use the GUI to make API calls

1. Click the extensions button **it's the last one** on VSCodium and search for Thunder in the text box, when the Thunder Client extension comes up, click install

![Extension Installation](images/install_thunder_extension.png)

2. Now search for C# and install the C# extension

![Extension Installation](images/install_csharp_extension.png)

3. There should now be a new icon on the leftmost toolbar

![Thunder Initial screen](images/thunder_init.png)

4. Make an initial request to see the API working

- Click the lightning bolt to get into the Thunder Client screen
- Click New Request
- In the address bar, to the right of GET, type in the http listening address your APi is running on followed by /weatherforecast for example `http://localhost:5132/weatherforecast` and click Send
- Your API should respond with some dummy Weather Forecasts!

5. Rejoice: your API is up and running! Now that we know our API project runs, lets go back to the terminal and kill the process by pressing `ctrl+c`. You should see `Application is shutting down...` and your prompt return

---

### Generate Build Assets

Since we have the C# extension installed, VSCodium can understand that we are looking at C# code in our project. The first time we open the existing `Program.cs` through VSCodium it detects this and asks us if we want to build the assets (generate json files with taks), we need to choose Yes.

![Generate Assets](images/generateassets.png)

If we missed this window we can still generate these by using VSCodium command pallete `ctrl+shift+p`: When the search bar comes up type Generate and the first command should be `.NET: Generate Assets for Build and Debug`. This should create a `.vscode` folder in our project, which we need to launch our api from the GUI instead of the terminal.

![Generate Assets Pallete](images/generateassets_pallete.png)

The generated folder should look as below

![Generate Assets Pallete](images/vscodeassets.png)

---

### Commit our changes

Since we want to have a starting and ending state for each lab for reference, let's go ahead and create a new branch and commit it with this final state of lab01

```bash
git checkout -b lab01_end
git add .
git commit -m "Lab01 finished"
```

---

That was a LOT! Have some more coffee and get ready to start coding.

<!-- OPTIONAL: References and Links -->
### References & Links

- [What is Git?](https://www.atlassian.com/git/tutorials/what-is-git)
- [VSCodium](https://vscodium.com/#intro)
- [ThunderClient for VSCodium](https://www.thunderclient.com/)

---

<!-- OPTIONAL: Include a navigator at the bottom -->
<!-- This is very user-friendly, but a pain to maintain -->

|Previous: [Main](/Readme.md)|Next: [Controllers](/labs/02_controllers/Readme.md)|
|---:|:---|
