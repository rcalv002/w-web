# Persistence

In the previous lab we set up a `Controller` and `Model` of our `Todo` item. Using Swagger we were able to easily retrieve a list of Todo items, and create or delete one. In reality this was all being faked and one request did not have anything to do with another. In this lab we'll be setting up a persistence layer so that we can retrieve, delete, and create Todo items that will persist from one api request to the next.

.Net Core uses an `Object Relational Mapper` called `Entity Framework Core`. What this really means is that we don't have to write SQL queries to INSERT, SELECT, DELETE our resources in a database table, instead we set up some rules and the system automatically generates functions for us to call that take care of the nitty-gritty.

`EF` is a complete topic and we'll only be using the very minimum we need to persist state.

---

## Environment

We'll be starting this lab where we left off the previous one inside `/home/<youruser>/TodoApi`. You should already have VSCodium open but if you don't go ahead and open it in this path

```bash
codium .
```

---

### Track it

Lets create our Lab03_start branch

```bash
git branch lab03_start
git checkout lab03_start
```

### Adding EntityFramework support

The first thing we need to do is add a package to our project. A package provides additional pre-built functionality that we don't have to write code for. We'll be adding the `Microsoft.EntityFrameworkCore.InMemory` package to our project so we can simulate an in-memory database persistence layer.

Find the `TodoApi.csproj` file in the root of the project and open it. This file has an XML based definition of what the project has in terms of dependencies and other configurations. Find the section `<ItemGroup>` and add the new package reference.

Before:

```csharp
  <ItemGroup>
    <PackageReference Include="Swashbuckle.AspNetCore" Version="6.2.3" />
  </ItemGroup>
```

After

```csharp
  <ItemGroup>
    <PackageReference Include="Swashbuckle.AspNetCore" Version="6.2.3" />
    <PackageReference Include="Microsoft.EntityFrameworkCore.InMemory" Version="6.0.3" />
  </ItemGroup>
```

Once you save the project file, VSCodium should pop a message for you saying that there are unresolved dependencies (meaning the package you just told the project it needs is not installed), and you should execute a project restore to install these. The popup looks like the image below. If you missed it you can open the Command Pallete with `ctrl+shift+p` and type restore

![Restore Package](images/packagerestore.png)
![Restore Package](images/restoreproject.png)

If neither of these works we can open a terminal in the project folder ***TodoApi*** and type `dotnet restore` so the package is installed.

We can now configure our project to use EntityFramework to connect our `database context`. For the purposes of this class, we'll be using an in-memory database.

The general steps are:

- Define the database context class. Access to our database flows through here
- Tell our project to use this context to access a specific database
- Change our TodoController to use the [Injected Dependency](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-6.0) DbContext to interact with our Todo data

#### Add DbContext

1. Click the `Models` folder and press the File+ icon to add the `TodoContext.cs` file. Paste the code below and save it. Notice how there's a `using` statement at the top to indicate that we are using the new package here. This is similar to ***declare*** or ***import*** in other languages. We are declaring a `DbContext` by which to access the database, and a `DbSet` (Table) to store in it.

```csharp
using Microsoft.EntityFrameworkCore;

namespace TodoApi.Models
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
            : base(options)
        {
        }

        public DbSet<TodoItem> TodoItems { get; set; } = null!;
    }
}
```

2. Open the `Program.cs` file in the root of the project and add the `TodoContext`.

Before:

```csharp

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
//.....

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
```

After:

```csharp
using Microsoft.EntityFrameworkCore;
using TodoApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
//.....

builder.Services.AddControllers();
builder.Services.AddDbContext<TodoContext>(opt => opt.UseInMemoryDatabase("TodoList"));
builder.Services.AddEndpointsApiExplorer();
```

---

### Using EntityFramework

Now that we have configured our project to support EntityFramework and have defined a context for our in-memory database, we'll need to update our File `TodoController.cs` in the `Controllers` folder to us this. There are a lot of changes so below is the final state of the controller, we can go over the changes in detail.

```csharp
// ./Controllers/TodoController.cs
// ./Controllers/TodoController.cs
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoApi.Models;

namespace TodoApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TodoController : ControllerBase
{
    private readonly ILogger<TodoController> _logger;
    private readonly TodoContext _dbcontext;

    public TodoController(ILogger<TodoController> logger, TodoContext dbcontext)
    {
        _logger = logger;
        _dbcontext = dbcontext;
    }

    [HttpGet()]
    public async Task<ActionResult<TodoItem[]>> GetTodoItems()
    {
        return await _dbcontext.TodoItems.ToArrayAsync();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<TodoItem>> GetTodoItem(long id)
    {
        var todoItem = await _dbcontext.TodoItems.FindAsync(id);

        if (todoItem == null)
        {
            return NotFound();
        }

        return todoItem;
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateTodoItem(long id, TodoItem todoItem)
    {
        if (id != todoItem.Id)
        {
            return BadRequest();
        }

        _dbcontext.Entry(todoItem).State = EntityState.Modified;

        try
        {
            await _dbcontext.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!TodoItemExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }

        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<TodoItem>> DeleteTodoItem(long id)
    {
        var todoItem = await _dbcontext.TodoItems.FindAsync(id);

        if (todoItem == null)
        {
            return NotFound();
        }

        _dbcontext.TodoItems.Remove(todoItem);
        await _dbcontext.SaveChangesAsync();

        return NoContent();
    }

    [HttpPost]
    public async Task<ActionResult<TodoItem>> CreateTodoItem(TodoItem todoItem)
    {
        _dbcontext.TodoItems.Add(todoItem);
        await _dbcontext.SaveChangesAsync();

        return CreatedAtAction(nameof(GetTodoItem), new { id = todoItem.Id }, todoItem);
    }

    private bool TodoItemExists(long id){
        return _dbcontext.TodoItems.Any(x => x.Id == id);
    }
}

```

You'll notice we have a using statement here to use our EntityFramework package

```csharp
using Microsoft.EntityFrameworkCore;
```

Our [Constructor](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/constructors), which initializes a class (our controller in this case) is now injecting the `TodoContext`, making it available for use in the Controller.

```csharp
public TodoController(ILogger<TodoController> logger, TodoContext dbcontext)
{
  _logger = logger;
  _dbcontext = dbcontext;
}
```

Instead of using a static selection or randomly built Todos, we now pull and push data to the in-memory database. This below is gone.

```csharp
private static TodoItem[] Todos = Enumerable.Range(1, 5).Select(index => new TodoItem
{
    Id = index,
    Name = Tasks[Random.Shared.Next(Tasks.Length)],
    IsComplete = false
}).ToArray();

```

If you want to practice writing the code or checking out [IntelliSense](https://code.visualstudio.com/docs/editor/intellisense) go ahead and type the changes otherwise, paste the final state of the file above and save it.

---

### Testing our changes

Previously we talked about [Swagger](https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-6.0&tabs=visual-studio). Since it keeps track of how our API looks, let's use it to send some requests to the API.

Start the project by going to the Debug tab and press play! If it was already running, stop it and start it again.
Open a browser to http://localhost:5000/swagger
In the swagger UI

- Expand the GET api/Todo
- Click Try it Out
- Click Execute

You should receive a success response (200) with an empty array []. That's because the database is currently empty. Press cancel and collapse the tab.

Let's add a Todo item:

- Expand the POST api/Todo
- Click Try it Out
- A Pane expands with the content this particular route expects to receive, this is our payload! Change the name property and click Execute

You should receive a 201 Successfully Created response with the content you sent in the payload, congratulations on adding your first Todo item.

Go back to the Get request from earlier and try it, you should now see an entry being returned.

The id property in our payload should be unique, in a real scenario we would designate this as an identity key that would auto-increment so we don't have to send it.

---

### Commit our changes

If the application is running, go back to the Debug tab on VSCodium and press stop.

Since we want to have a starting and ending state for each lab for reference, let's go ahead and create a new branch and commit it with this final state of lab03

```bash
git checkout -b lab03_end
git add .
git commit -m "Lab03 finished"
```

---

That was a LOT! Have some more coffee.

<!-- OPTIONAL: References and Links -->
### References & Links

- [Entity Framework DbContext](https://docs.microsoft.com/en-us/ef/core/dbcontext-configuration/)
- [Dependency Injection](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-6.0)

---

<!-- OPTIONAL: Include a navigator at the bottom -->
<!-- This is very user-friendly, but a pain to maintain -->

|Previous: [Controllers](/labs/02_controllers/Readme.md)|Next: [Optimizing Data Transfers](/labs/04_dto/Readme.md)|
|---:|:---|
