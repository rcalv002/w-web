<!-- Update the lab title -->
# Controllers

<!-- Reminder: -->
<!-- Labs should take ~60 minutes -->
<!-- ~25% Talk: What and why. -->
<!-- ~25% Demo: What and how. -->
<!-- ~50% Do: Pair and share, redoing the demo -->

<!-- IF THIS IS THE LAST LAB, PUT FEEDBACK REMINDERS AT THE START -->

<!-- Update this sub-header -->
## Why do we need controllers?
<!-- 3-4 sentences -->
<!-- Answer the following questions -->
Web/API hosts are services listening for client requests. These requests have a general shape comprising the `method` and a `resource path`.

Common methods include

- GET: Used to retrieve a resource
- PATCH: Used to partially update a resource
- PUT: Used to replace a resource completely
- POST: Used to create a resource
- DELETE: Used to delete a resource

A resource could be a

- Customer
- Job
- Person
- Order
- Etc.

This looks like `METHOD scheme://hostaddress/resourcepath`
A request that comes into a web host on a particular resource path using different methods should perform different functions. Consider the examples below:

The request `GET http://localhost/path` will try to retrieve information from the resource `path`

The request `POST https://localhost/path` will try to push information to the resource `path`

Behind the scenes each of these is a unique function to execute.
In general GET functions do not receive content, but POST functions must have content, because they need to know what the system is receiving.

Controllers have a simple job: they map our API requests using the `path` and the `method` to a function.

---

<!-- OPTIONAL Section -->
## Environment

We'll be starting this lab where we left off inside `/home/<youruser>/TodoApi`. You should already have VSCodium open but if you don't, go ahead and open it in this path.

```bash
codium .
```

---

### Track it

Let's create our Lab02_start branch

```bash
git checkout -b lab02_start
```

<!-- Update this section name -->
### Updating our scheme

Before we start, we want to remove https support from our project, because we don't have a real certificate and we don't want to deal with installing custom certificates. We'll just serve our API on http so our Web Browser doesn't complain.

1. Open `Program.cs`, find the line below and delete or comment it, then save the `Program.cs` file by pressing `ctrl+s` or clicking `File -> Save`. Single line comments in `.cs` files are done by prepending // to the beginning of the line.

```csharp
//app.UseHttpsRedirection();
```

2. We can now run our project from inside VSCodium and then launch `Firefox` against it

- Click the `Debug` icon, it has a bug and a play icon on it
- On the Debug window, the dropdown next to RUN should say `.NET Core Launch` which is the default profile created in our json file earlier when we built the assets
- Press the play button and several things happen
  - A debug toolbar appears at the same level as the Play button: it has Pause, Restart, Stop, etc. icons. The application can be stopped by pressing the square button
  - On the bottom of the screen, the terminal, output, and debug console opens
  - The Debug Console displays debug logs from the running application, the same ones we saw on the terminal in Lab 01.
  - The difference is that the launch profile from VSCodium sets the default port for http to `5000`
- Open `FireFox` and browse to http://localhost:5000/weatherforecast - we should see dummy weather forecasts!
- Press the Stop button on the Debug Toolbar to stop the running process

3. Let's commit here!

```bash
git add .
git commit -m "Lab02_start"
```

---

### Creating our TodoController and Data Model

The Weather Forecast sample controller is great, but we'll be creating and expanding on our own Controller throughout our labs.

1. Create a `Models` Folder inside our `TodoApi` folder by clicking the Folder+ icon

![Create Models Folder](images/create_models_folder.png)

2. Click the `Models` Folder and create a `TodoItem.cs` File by clicking the File+ icon

![Create Todo Model](images/create_todo_model.png)

3. Our `TodoItem.cs` file defines a namespace (to group our classes) and has a single class in it. 
It is a model of what a single `TodoItem` should look like.
Paste the code below into the file and save it

```csharp
namespace TodoApi.Models
{
    public class TodoItem
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public bool IsComplete { get; set; }
    }
}
```

4. We'll be adding a Controller next. The Controller defines the functions we mentioned earlier that map to the `Method` (GET,POST,DELETE,PATCH). You'll see before each private function a `Decorator` that indicates which `Method` this function handles. .Net does a lot of this for us by convention so we don't have to wire everything up manually, we simply decorate our functions. Click the `Controllers` Folder and create a `TodoController.cs` File by clicking the File+ icon

![Create Todo Controller](images/create_todo_controller.png)

Paste the code below into the file and `save it`

```csharp
using Microsoft.AspNetCore.Mvc;
using TodoApi.Models;

namespace TodoApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TodoController : ControllerBase
{
    private static string[] Tasks = new[]
    {
        "Eat food", "Sleep", "Chill", "Dance", "Learn"
    };

    private static TodoItem[] Todos = Enumerable.Range(1, 5).Select(index => new TodoItem
    {
        Id = index,
        Name = Tasks[Random.Shared.Next(Tasks.Length)],
        IsComplete = false
    }).ToArray();

    private readonly ILogger<TodoController> _logger;

    public TodoController(ILogger<TodoController> logger)
    {
        _logger = logger;
    }

    [HttpGet()]
    public async Task<ActionResult<TodoItem[]>> GetTodoItems()
    {
        return await Task.FromResult(Todos);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<TodoItem>> GetTodoItem(long id)
    {
        var todo = Todos.FirstOrDefault(t => t.Id == id);
        if(todo is null)
            return NotFound();
        
        return await Task.FromResult(todo);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<TodoItem>> DeleteTodoItem(long id)
    {
        // We currently are not storing data anywhere so lets just return no content.
        return await Task.FromResult(NoContent());
    }

    [HttpPost]
    public async Task<ActionResult<TodoItem>> PostTodoItem(TodoItem todoItem)
    {
        // We currently are not storing data anywhere so lets just return the posted object.
        return await Task.FromResult(CreatedAtAction(nameof(GetTodoItem), new { id = todoItem.Id }, todoItem));
    }
}

```

5. We've added a new Model and a controller that interacts with that model. 
Go back to the Debug tab and run the process again by pressing the Play button as before. 
On FireFox, lets visit `http://localhost:5000/api/todo`. We should get some random Todos. Nice!

---

### Swagger

Its nice that we can curl, wget, browse to the url, or use our built-in Thunder client to interact with the API. However, updating our api calls can become cumbersome as we change api paths or paremeters. Fear not! There's a community project called Swagger that has been adopted and incorporated into the .Net starter templates. Swagger reads your project and finds api endpoints, reads their signatures and provides you an easy to use GUI to test these. It makes it much easier during development!

1. Open your browser and head to `http://localhost:5000/swagger`
2. Choose any of the entries and click Try it out!
3. Enter any necessary paremeters and execute the endpoint

![Create Todo Controller](images/swagger.png)

When you're done playing, close the browser and go back to the Debug tab on VSCodium and press stop to end the running application.

---

### Commit our changes

If the application is running, go back to the Debug tab on VSCodium and press stop.

Since we want to have a starting and ending state for each lab for reference, let's go ahead and create a new branch and commit it with this final state of lab02

```bash
git checkout -b lab02_end
git add .
git commit -m "Lab02 finished"
```

---

That was a LOT! Have some more coffee.

<!-- OPTIONAL: References and Links -->
### References & Links

- [Swagger/Swashbuckle](https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-6.0&tabs=visual-studio)
- [ASP.NET Core MVC Controllers](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/actions?view=aspnetcore-6.0)

---

<!-- OPTIONAL: Include a navigator at the bottom -->
<!-- This is very user-friendly, but a pain to maintain -->

|Previous: [Project Setup](/labs/01_project_setup/Readme.md)|Next: [Persistence](/labs/03_persistence/Readme.md)|
|---:|:---|
